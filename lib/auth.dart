import 'package:flutter/material.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:validators/validators.dart';

class AutorizationPage extends StatefulWidget {
  const AutorizationPage({Key? key}) : super(key: key);

  @override
  State<AutorizationPage> createState() => _AutorizationPageState();
}

class _AutorizationPageState extends State<AutorizationPage> {
  final TextEditingController _phonecontroller = TextEditingController();
  final TextEditingController _passwordcontroller = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  String? inputtedValue;
  String value = "";
  String value_2 = "";
  bool _isHidden = true;
  bool userInteracts() => inputtedValue != null;

  late String _phone;
  late String _password;

  void _togglePasswordView() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget _logo() {
      return Padding(
        padding: const EdgeInsets.only(top: 100),
        child: Container(
          child: const Align(
            child: Text(
              'WELCOME',
              style: TextStyle(
                  fontSize: 45,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
          ),
        ),
      );
    }

    Widget _inputlogin(Icon icon, String hint, TextEditingController controller,
        bool obscure) {
      return Container(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: IntlPhoneField(
          initialCountryCode: 'RU',
          onChanged: (phone) {
            print(phone.completeNumber);
          },
          keyboardType: TextInputType.number,
          controller: controller,
          obscureText: obscure,
          style: const TextStyle(fontSize: 20, color: Colors.white),
          decoration: InputDecoration(
              hintStyle: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Colors.white30),
              hintText: hint,
              focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 3)),
              enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white54, width: 3)),
              prefixIcon: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: IconTheme(
                    data: const IconThemeData(color: Colors.white),
                    child: icon,
                  ))),
        ),
      );
    }

    Widget _inputpassword(Icon icon, String hint,
        TextEditingController controller, bool obscure) {
      return Container(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: TextFormField(
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Поле пароля не может быть пустым';
            }
            if (value.length < 8) {
              return 'Пароль не может быть меньше 8 символов';
            }
            return null;
          },
          onChanged: (value) => setState(() {
            inputtedValue = value;
          }),
          controller: controller,
          obscureText: _isHidden,
          style: const TextStyle(fontSize: 20, color: Colors.white),
          decoration: InputDecoration(
              hintStyle: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Colors.white30),
              hintText: hint,
              suffix: InkWell(
                onTap: _togglePasswordView,
                child:
                    Icon(_isHidden ? Icons.visibility : Icons.visibility_off),
              ),
              focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 3)),
              enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white54, width: 3)),
              prefixIcon: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: IconTheme(
                    data: const IconThemeData(color: Colors.white),
                    child: icon,
                  ))),
        ),
      );
    }

    Widget _button(String text, void Function() func) {
      return RaisedButton(
        splashColor: Theme.of(context).primaryColor,
        highlightColor: Theme.of(context).primaryColor,
        color: Colors.white,
        child: Text(text,
            style: const TextStyle(
                fontWeight: FontWeight.bold, color: Colors.blue, fontSize: 20)),
        onPressed: !userInteracts() ||
                _formKey.currentState == null ||
                !_formKey.currentState!.validate()
            ? null
            : () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                setState(() {
                  value = prefs.getString("Phone") ?? "NULL";
                  value_2 = prefs.getString("Password") ?? "NULL";
                });
                prefs.setString("Phone", value);
                prefs.setString("Password", value_2);
              },
      );
    }

    Widget _form(String lable, void Function() func) {
      return Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(bottom: 20, top: 70),
                  child: _inputlogin(const Icon(Icons.phone), 'XXX XXX XX XX',
                      _phonecontroller, false)),
              Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: _inputpassword(const Icon(Icons.lock), 'Password',
                      _passwordcontroller, true)),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  child: _button(lable, func),
                ),
              )
            ],
          ));
    }

    void _buttonAction() {
      _phone = _phonecontroller.text;
      _password = _passwordcontroller.text;

      _phonecontroller.clear();
      _passwordcontroller.clear();
    }

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColorDark,
      body: Column(
        children: [_logo(), _form('ВОЙТИ', _buttonAction)],
      ),
    );
  }
}
