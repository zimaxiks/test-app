import 'package:flutter/material.dart';
import 'package:flutter_application_1/auth.dart';
import 'package:flutter_application_1/homepage.dart';

class Check extends StatelessWidget {
  const Check({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const bool iSLoggedIn = true;

    return iSLoggedIn ? const HomePage() : const AutorizationPage();
  }
}
