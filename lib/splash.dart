import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/check.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    const String assetNamepath = 'assets/images/pokemon_PNG23.png';

    Image imageFromAsset = Image.asset(
      assetNamepath,
      fit: BoxFit.cover,
    );
    Timer(const Duration(seconds: 3), () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => const Check()));
    });
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(),
            Column(
              children: [
                imageFromAsset,
                const SpinKitDoubleBounce(
                  color: Colors.white,
                  size: 100,
                )
              ],
            ),
            const Text(
              'powered by Flutter\n',
              style: TextStyle(color: Colors.orange),
            )
          ],
        ),
      ),
    );
  }
}
